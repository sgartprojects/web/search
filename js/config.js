class Config {
	constructor() {}

	getThemeMode() {
		const themeModes = {
			'light': {
				name: 'Light',
				icon: 'light-mode'
			},
			'dark': {
				name: 'Dark',
				icon: 'dark-mode'
			},
			'auto': {
				name: 'Auto',
				icon: 'auto-mode',
				lightHour: '7',
				darkHour: '19'
			}
		}

		return themeModes;
	}

	getQuickSearchData() {
		const quickSearchData = {
			'r/': {
				urlPrefix: 'https://reddit.com/r/'
			},
			'w/': {
				urlPrefix: 'https://wikipedia.org/wiki/'
			},
			'u/': {
				urlPrefix: 'https://unsplash.com/s/photos/'
			},
			'a/': {
				urlPrefix: 'https://amazon.com/s?k='
			},
			'y/': {
				urlPrefix: 'https://youtube.com/results?search_query='
			},
			'g/': {
				urlPrefix: 'https://git.sgart.io/search?q='
			},
			'd/': {
				urlPrefix: 'https://digitec.ch/en/search?q='
			},
			't/': {
				urlPrefix: 'https://www.thingiverse.com/search?q='
			}
		};

		return quickSearchData;
	}

	getSearchEngines() {

		const searchEngines = {
			'ecosia': {
				name: 'Ecosia',
				prefix: 'https://www.ecosia.org/search?q=',
				icon: 'ecosia'
			},
			'duckduckgo': {
				name: 'Duckduckgo',
				prefix: 'https://duckduckgo.com/?q=',
				icon: 'duckduckgo'
			},
			'google': {
				name: 'Google',
				prefix: 'https://www.google.com/search?q=',
				icon: 'google'
			}
		};

		return searchEngines;
	}

	getWebSites() {
		// Web menu
		// A list of websites that will be generated and put on the web menu
		const webSites = [
			{
				site: 'Reddit',
				icon: 'reddit',
				url: 'https://reddit.com/',
				category: 'social'
			},
			{
				site: 'Youtube',
				icon: 'youtube',
				url: 'https://youtube.com/',
				category: 'media'
			},
			{
				site: 'Twitter',
				icon: 'twitter',
				url: 'https://twitter.com/',
				category: 'social'
			},
			{
				site: 'Bitbucket',
				icon: 'bitbucket',
				url: 'https://bitbucket.org/',
				category: 'development'
			},
			{
				site: 'Gitlab',
				icon: 'gitlab',
				url: 'https://git.sgart.io/',
				category: 'development'
			},
			{
				site: 'Duckduckgo',
				icon: 'duckduckgo',
				url: 'https://duckduckgo.com/',
				category: 'search Engine'
			},
			{
				site: 'Ecosia',
				icon: 'ecosia',
				url: 'https://ecosia.org/',
				category: 'search Engine'
			},
			{
				site: 'Google',
				icon: 'google',
				url: 'https://google.com/',
				category: 'search Engine'
			},
			{
				site: 'Wikipedia',
				icon: 'wikipedia',
				url: 'https://wikipedia.org/',
				category: 'information'
			},
			{
				site: 'Unsplash',
				icon: 'unsplash',
				url: 'https://unsplash.com/',
				category: 'design'
			},
			{
				site: 'Twitch',
				icon: 'twitch',
				url: 'https://twitch.tv/',
				category: 'media'
			},
			{
				site: 'Material.io',
				icon: 'materialio',
				url: 'https://material.io/',
				category: 'design'
			},
			{
				site: 'Office 365',
				icon: 'office365',
				url: 'https://office.com/',
				category: 'information'
			},
			{
				site: 'Discord',
				icon: 'discord',
				url: 'https://discord.com/',
				category: 'social'
			},
			{
				site: 'Spotify',
				icon: 'spotify',
				url: 'https://spotify.com/',
				category: 'media'
			},
			{
				site: 'Stackoverflow',
				icon: 'stackoverflow',
				url: 'https://stackoverflow.com/',
				category: 'development'
			},
			{
				site: 'Markdown Cheatsheet',
				icon: 'markdown',
				url: 'https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet/',
				category: 'development'
			},
			{
				site: 'Home Assistant',
				icon: 'homeassistant',
				url: 'https://home.sgart.io/',
				category: 'smart Home'
			},
			{
				site: 'Nextcloud',
				icon: 'nextcloud',
				url: 'https://cloud.sgart.io/',
				category: 'cloud'
			},
			{
				site: 'Portainer',
				icon: 'docker',
				url: 'https://portainer.sgart.io/',
				category: 'development'
			},
			{
				site: 'Octoprint',
				icon: '3dprinter',
				url: 'https://oktoprint.sgart.io/',
				category: '3D Printing'
			},
			{
				site: 'Thingiverse',
				icon: 'thingiverse',
				url: 'https://thingiverse.com/',
				category: '3D Printing'
			},
			{
				site: 'Bitwarden',
				icon: 'bitwarden',
				url: 'https://bitwarden.com/',
				category: 'security'
			},
			{
				site: 'Threema',
				icon: 'threema',
				url: 'https://web.threema.ch',
				category: 'social'
			},
			{
				site: 'Leo',
				icon: 'leo',
				url: 'https://dict.leo.org',
				category: 'information'
			},
			{
				site: 'JetBrains',
				icon: 'jetbrains',
				url: 'https://jetbrains.com',
				category: 'development'
			},
			{
				site: 'Digitec',
				icon: 'digitec',
				url: 'https://digitec.ch',
				category: 'shopping'
			},
			{
				site: 'V-Zug',
				icon: 'vzug',
				url: 'https://vzug.com',
				category: 'shopping'
			},
			{
				site: 'Outlook',
				icon: 'outlook',
				url: 'https://outlook.office.com',
				category: 'social'
			},
			{
				site: 'Pixabay',
				icon: 'pixabay',
				url: 'https://pixabay.com',
				category: 'design'
			},
			{
				site: 'Plex',
				icon: 'plex',
				url: 'https://plex.sgart.io',
				category: 'media'
			}
		];

		return webSites;
	}

	getPanelSites() {
		// Panel
		// A list of websites that will be generated and put on the Panel
		const panelSites = [
			{
				site: 'HomeAssistant',
				icon: 'homeassistant',
				url: 'https://home.sgart.io/'
			},
			{
				site: 'Cloud',
				icon: 'nextcloud',
				url: 'https://cloud.sgart.io/'
			},
			{
				site: 'Portainer',
				icon: 'docker',
				url: 'https://portainer.sgart.io/'
			},
			{
				site: 'Gitlab',
				icon: 'gitlab',
				url: 'https://git.sgart.io/'
			},
			{
				site: 'Plex',
				icon: 'plex',
				url: 'https://plex.sgart.io',
				category: 'media'
			},
			{
				site: 'Digitec',
				icon: 'digitec',
				url: 'https://digitec.ch/'
			}
		];

		return panelSites;
	}
}
